---
description: "Places to socialise with fellow Ballafornians!"
---

Below are some links where you can socialise online with fellow residents of
Ballarat. If you know of more or have links of your own to add please submit
them via
[gitlab](https://gitlab.com/ballarat-hackerspace/websites/ballarat.social/-/issues)
or [email](mailto:committee@ballarathackerspace.org.au) them to us.

This site is provided by the [Ballarat
Hackerspace](https://ballarathackerspace.org.au), if you have any interest in
electronics, robotics, 3d printing, programming or many other related topics be
sure to drop in and say hello! :heart:

Check out [ballarat.chat](https://ballarat.chat) if you're looking for places to chat.
